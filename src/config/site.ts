import type { FooterItem, MainNavItem } from "@/types"

import { productCategories } from "@/config/products"
import { slugify } from "@/lib/utils"

export type SiteConfig = typeof siteConfig

const links = {
  twitter: "https://twitter.com/sadmann17",
  github: "https://github.com/sadmann7/skateshop",
  githubAccount: "https://github.com/sadmann7",
  discord: "https://discord.com/users/sadmann7",
  calDotCom: "https://cal.com/sadmann7",
}

export const siteConfig = {
  name: "ВЫКСА МАРКЕТ",
  description:
    "онлайн шоурум, который объединяет предпринимателей, создающих уникальные проекты, за которыми стоят реальные люди и их любовь к своему делу.",
  url: "https://skateshop.sadmn.com",
  ogImage: "https://skateshop.sadmn.com/opengraph-image.png",
  links,
  mainNav: [
    {
      title: "О нас",
      items: [
        {
          title: "Продукты",
          href: "/products",
          description: "Все продукты, которые мы можем предложить.",
          items: [],
        },
        // {
        //   title: "Build a Board",
        //   href: "/build-a-board",
        //   description: "Build your own custom skateboard.",
        //   items: [],
        // },
        {
          title: "Блог",
          href: "/blog",
          description: "Прочтите наши последние публикации в блоге.",
          items: [],
        },
      ],
    },
    ...productCategories.map((category) => ({
      title: category.navName,
      items: [
        {
          title: "Товары",
          href: `/categories/${slugify(category.title)}`,
          description: `Все товары`,
          items: [],
        },
        ...category.subcategories.map((subcategory) => ({
          title: subcategory.title,
          href: `/categories/${slugify(category.title)}/${subcategory.slug}`,
          description: subcategory.description,
          items: [],
        })),
      ],
    })),
  ] satisfies MainNavItem[],
  footerNav: [
    {
      title: "Помощь",
      items: [
        {
          title: "О нас",
          href: "/about",
          external: false,
        },
        {
          title: "Контакты",
          href: "/contact",
          external: false,
        },
        {
          title: "Политика конф.",
          href: "/terms",
          external: false,
        },
        {
          title: "Приватность",
          href: "/privacy",
          external: false,
        },
      ],
    },
    {
      title: "Социальные сети",
      items: [
        {
          title: "Twitter",
          href: links.twitter,
          external: true,
        },
        {
          title: "GitHub",
          href: links.githubAccount,
          external: true,
        },
        {
          title: "Discord",
          href: links.discord,
          external: true,
        },
        {
          title: "cal.com",
          href: links.calDotCom,
          external: true,
        },
      ],
    },
  ] satisfies FooterItem[],
}
