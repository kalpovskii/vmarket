export const storeSortOptions = [
  {
    label: "По кол-ву товаров",
    value: "productCount.desc",
  },
  {
    label: "По новизне",
    value: "createdAt.desc",
  },
  {
    label: "По алфавиту",
    value: "name.asc",
  },
]

export const storeStatusOptions = [
  { label: "Active", value: "active" },
  { label: "Inactive", value: "inactive" },
]
