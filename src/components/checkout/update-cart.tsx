"use client"

import * as React from "react"
import type { CartLineItem } from "@/types"
import { MinusIcon, PlusIcon, TrashIcon } from "@radix-ui/react-icons"

import { catchError } from "@/lib/utils"
import { Button } from "@/components/ui/button"
import { Input } from "@/components/ui/input"
import { deleteCartItemAction, updateCartItemAction } from "@/app/_actions/cart"

interface UpdateCartProps {
  cartLineItem: CartLineItem
}

export function UpdateCart({ cartLineItem }: UpdateCartProps) {
  const id = React.useId()
  const [isPending, startTransition] = React.useTransition()

  return (
    <div className="flex w-full items-center justify-between space-x-2 xs:w-auto xs:justify-normal">
      <Button
        id={`${id}-delete`}
        variant="outline"
        size="icon"
        className="h-8 w-8"
        onClick={() => {
          startTransition(async () => {
            try {
              await deleteCartItemAction({
                productId: cartLineItem.id,
              })
            } catch (err) {
              catchError(err)
            }
          })
        }}
        disabled={isPending}
      >
        <TrashIcon className="h-3 w-3" aria-hidden="true" />
        <span className="sr-only">Удалить товар</span>
      </Button>
    </div>
  )
}
