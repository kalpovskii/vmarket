import Link from "next/link"
import { type CuratedStore } from "@/types"

import { getRandomPatternStyle } from "@/lib/generate-pattern"
import { cn } from "@/lib/utils"
import { AspectRatio } from "@/components/ui/aspect-ratio"
import { Badge } from "@/components/ui/badge"
import {
  Card,
  CardDescription,
  CardHeader,
  CardTitle,
} from "@/components/ui/card"
import Image from "next/image";
import * as React from "react";

interface StoreCardProps {
  store: CuratedStore
  href: string
}

export function StoreCard({ store, href }: StoreCardProps) {
  return (
    <Link href={href}>
      <Card className="h-full overflow-hidden">
        <AspectRatio ratio={21 / 9}>
          <Image
              src="/images/clothing-one.webp"
              alt="store logo"
              className="object-cover"
              sizes="(min-width: 1024px) 20vw, (min-width: 768px) 25vw, (min-width: 640px) 33vw, (min-width: 475px) 50vw, 100vw"
              fill
              loading="lazy"
          />
        </AspectRatio>
        <CardHeader>
          <CardTitle className="line-clamp-1 text-lg">{store.name}</CardTitle>
          <CardDescription className="line-clamp-2">
            {store.description?.length
              ? store.description
              : `Товары магазина ${store.name}`}
          </CardDescription>
        </CardHeader>
      </Card>
      <span className="sr-only">{store.name}</span>
    </Link>
  )
}
